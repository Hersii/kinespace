set(ENV{PYTHONPATH} ${CMAKE_CURRENT_SOURCE_DIR}/python)

option(PYTHON_QT_DYNAMIC_LOAD_UI
    "Load ui files dynamically instead of \"pre-compiled\" modules")

if(NOT PYTHON_QT_DYNAMIC_LOAD_UI)
    message(STATUS "Compiling UI modules")

    execute_process(
        COMMAND python compile_ui_files.py ${CMAKE_CURRENT_SOURCE_DIR}/python/gui/design --directory
        WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/python/utils
        RESULT_VARIABLE UI_COMPILE_RESULT
        OUTPUT_VARIABLE UI_COMPILE_STDOUT
        ERROR_VARIABLE UI_COMPILE_STDERR
    )

    string(STRIP "${UI_COMPILE_STDOUT}" UI_COMPILE_STDOUT)
    string(STRIP "${UI_COMPILE_STDERR}" UI_COMPILE_STDERR)

    if(UI_COMPILE_RESULT EQUAL "0")
        if(NOT UI_COMPILE_STDERR STREQUAL "")
            message(WARNING "UI compilation warnings:\n${UI_COMPILE_STDERR}")
        endif()

        message(VERBOSE "UI compilation messages:\n${UI_COMPILE_STDOUT}")

    else()
        message(SEND_ERROR "UI compilation failed\n${UI_COMPILE_STDERR}")
    endif()
endif()
