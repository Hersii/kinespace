#version 450
in vec3 pos;
in vec3 col;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

out gl_PerVertex
{
    vec4 gl_Position;
};
out vec4 vert_col;

void main() {
  gl_Position = projection * view * model * vec4(pos, 1.0f);
  vert_col = vec4(col, 1.0f);
}