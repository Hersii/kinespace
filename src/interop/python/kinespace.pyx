#!/usr/bin/env python
from libc.stdint cimport *
import numpy as np
cimport numpy as npc

cdef extern from "stdlib.h":
    void free(void *ptr)

cdef extern from "kinespace.h":
    ...