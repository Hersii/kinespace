import numpy as np
from pyrr.matrix44 import create_orthogonal_projection, create_perspective_projection

def frustum(top, bottom, right, left, near, far, dtype=np.float32):
    width = (right - left)
    height = (top - bottom)
    depth = (far - near)
    a = (2 * near) / height
    b = (2 * near) / width
    c = (right + left) / width
    d = (top + bottom) / height
    e = -(far + near) / depth
    f = -(2 * far * near) / depth
    return np.array((
        (a, 0, 0, 0),
        (0, b, 0, 0),
        (c, d, e, -1),
        (0, 0, f, 0)
    ), dtype=dtype)

def perspective(width, height, x_fov, z_near, z_far, dtype=np.float32):

    aspect = width / height
    right = z_near * np.math.tan((np.math.pi * x_fov) / 360)
    left = -right
    top = right * aspect
    bottom = -top
    return frustum(top, bottom, right, left, z_near, z_far, dtype=dtype)

    # commented out for debugging
    # return create_perspective_projection(y_fov, aspect, z_near, z_far, dtype=np.float32)

def orthographic(x_size, y_size, z_near, z_far, dtype=np.float32):
    return create_orthogonal_projection(-x_size/2, x_size/2, -y_size/2, y_size/2, z_near, z_far, dtype=dtype)
