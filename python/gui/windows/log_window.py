import logging
import os.path as path
import re

from PyQt5.QtCore import Qt, pyqtSignal
from PyQt5.QtGui import QColor, QKeySequence, QSyntaxHighlighter, QTextCharFormat, QTextDocument
from PyQt5.QtWidgets import QAction, QActionGroup, QFileDialog, QMainWindow, QMenu, QPlainTextEdit

from utils.log_factory import create_class_logger


class LogPlainTextEdit(logging.Handler, QPlainTextEdit):
    appendLog = pyqtSignal(str)

    def __init__(self, level=logging.NOTSET, parent=None):
        logging.Handler.__init__(self, level)
        QPlainTextEdit.__init__(self, parent)
        self.setReadOnly(True)
        self.appendLog.connect(self.appendPlainText)

    def emit(self, record):
        message = self.format(record)
        self.appendLog.emit(message)


class LogSyntaxHighlighter(QSyntaxHighlighter):
    def __init__(self, document: QTextDocument):
        super().__init__(document)

        self.debug_format = QTextCharFormat()
        self.debug_format.setForeground(Qt.lightGray)
        self.warning_format = QTextCharFormat()
        self.warning_format.setForeground(QColor.fromHsv(30, 255, 255))  # orange
        self.error_format = QTextCharFormat()
        self.error_format.setForeground(Qt.darkRed)
        self.fatal_format = QTextCharFormat()
        self.fatal_format.setForeground(Qt.red)
        self.critical_format = QTextCharFormat()
        self.critical_format.setBackground(Qt.red)

    def highlightBlock(self, text):
        pattern = r"^.*\[([A-Z]+)\].*$"
        match = re.search(pattern, text, re.MULTILINE)
        if match:
            if match.group(1) == "DEBUG":
                self.setFormat(match.start(), match.end(), self.debug_format)
            if match.group(1) == "WARNING":
                self.setFormat(match.start(), match.end(), self.warning_format)
            if match.group(1) == "ERROR":
                self.setFormat(match.start(), match.end(), self.error_format)
            if match.group(1) == "FATAL":
                self.setFormat(match.start(), match.end(), self.fatal_format)
            if match.group(1) == "CRITICAL":
                self.setFormat(match.start(), match.end(), self.critical_format)


class LogWindow(QMainWindow):
    def __init__(self, parent=None, flags=Qt.WindowFlags()):
        super().__init__(parent=parent, flags=flags)

        self.file_menu = QMenu("File", self.menuBar())
        self.action_copy = QAction("Copy", self.file_menu)
        self.action_save = QAction("Save...", self.file_menu)
        self.action_clear = QAction("Clear", self.file_menu)

        self.options_menu = QMenu("Options", self.menuBar())
        self.severity_group = QActionGroup(self.options_menu)

        self.log_debug_action = QAction("Debug", self.severity_group)
        self.log_info_action = QAction("Info", self.severity_group)
        self.log_warn_action = QAction("Warning", self.severity_group)
        self.log_error_action = QAction("Error", self.severity_group)
        self.log_fatal_action = QAction("Fatal", self.severity_group)
        self.log_critical_action = QAction("Critical", self.severity_group)

        self.log_textbox = LogPlainTextEdit(parent=self)
        self.log_highlight = LogSyntaxHighlighter(self.log_textbox.document())

        self._init_actions()
        self._init_menus()
        self._init_textbox()

    def __del__(self):
        logging.getLogger().removeHandler(self.log_textbox) # remove root handler upon window destruction

    def _init_actions(self):
        self.action_save.setShortcut(QKeySequence.Copy)
        self.action_save.triggered.connect(self.log_textbox.selectAll)
        self.action_save.triggered.connect(self.log_textbox.copy)

        self.action_save.setShortcut(QKeySequence.Save)
        self.action_save.triggered.connect(self.save_output)

        self.action_clear.triggered.connect(self.log_textbox.clear)

        self.log_debug_action.triggered.connect(lambda: logging.getLogger().setLevel(logging.DEBUG))
        self.log_info_action.triggered.connect(lambda: logging.getLogger().setLevel(logging.INFO))
        self.log_warn_action.triggered.connect(lambda: logging.getLogger().setLevel(logging.WARNING))
        self.log_error_action.triggered.connect(lambda: logging.getLogger().setLevel(logging.ERROR))
        self.log_fatal_action.triggered.connect(lambda: logging.getLogger().setLevel(logging.FATAL))
        self.log_critical_action.triggered.connect(lambda: logging.getLogger().setLevel(logging.CRITICAL))

        self.severity_group.addAction(self.log_debug_action)
        self.severity_group.addAction(self.log_info_action)
        self.severity_group.addAction(self.log_warn_action)
        self.severity_group.addAction(self.log_error_action)
        self.severity_group.addAction(self.log_fatal_action)
        self.severity_group.addAction(self.log_critical_action)
        self.log_info_action.setChecked(True)
        self.log_info_action.trigger()

    def _init_menus(self):
        self.file_menu.addAction(self.action_save)
        self.file_menu.addAction(self.action_clear)

        self.options_menu.addAction(self.log_debug_action)
        self.options_menu.addAction(self.log_info_action)
        self.options_menu.addAction(self.log_warn_action)
        self.options_menu.addAction(self.log_error_action)
        self.options_menu.addAction(self.log_fatal_action)
        self.options_menu.addAction(self.log_critical_action)

        self.menuBar().addAction(self.file_menu.menuAction())
        self.menuBar().addAction(self.options_menu.menuAction())

    def _init_textbox(self):
        root_logger = logging.getLogger()
        formatter = logging.Formatter('[{levelname}] {asctime} | {name}.{funcName}: {message}', style='{')

        # root widget logging handler
        self.setCentralWidget(self.log_textbox)
        self.log_textbox.setFormatter(formatter)
        root_logger.addHandler(self.log_textbox) # set handler on global root logger

        # root stream handler
        root_streamhdlr = logging.StreamHandler()
        root_streamhdlr.setFormatter(formatter)
        root_logger.addHandler(root_streamhdlr)
        root_logger.setLevel(logging.DEBUG)

        # class logger member
        self.logger = create_class_logger(LogWindow)
        self.logger.debug("Log window initialized")

    def save_output(self):
        filename, file_filter = QFileDialog.getSaveFileName(self, "Save Log", path.expanduser("~"), "Log files (*.log)")
        with open(path.normpath(filename), mode="w") as file:
            self.logger.info('Log file saved to "{}"', filename)
            file.write(self.log_textbox.toPlainText())
