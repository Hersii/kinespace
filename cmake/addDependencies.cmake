message(STATUS "Processing dependencies...")

find_package(Git REQUIRED)

include(ExternalProject)

set_directory_properties(PROPERTIES EP_PREFIX "ext")

macro(configure_dependency)
  # get all extra arguments passed to macro
  set(options SEPARATE_PLATFORMS)
  set(oneValueArgs TARGET PKG_VARNAME DESCRIPTION)
  cmake_parse_arguments(CONFIGURE_DEPENDENCY "${options}" "${oneValueArgs}" "" ${ARGN})
  if(CONFIGURE_DEPENDENCY_UNPARSED_ARGUMENTS)
    message(FATAL_ERROR "configure_dependency: Unexpected arguments (${CONFIGURE_DEPENDENCY_UNPARSED_ARGUMENTS})")
  endif()
  if(CONFIGURE_DEPENDENCY_KEYWORDS_MISSING_VALUES)
    message(FATAL_ERROR "configure_dependency: Missing values for keywords (${CONFIGURE_DEPENDENCY_UNPARSED_ARGUMENTS})")
  endif()
  if(NOT DEFINED CONFIGURE_DEPENDENCY_TARGET)
    message(FATAL_ERROR "configure_dependency: You must define TARGET")
  endif()
  if(NOT DEFINED CONFIGURE_DEPENDENCY_DESCRIPTION)
    message(FATAL_ERROR "configure_dependency: You must define DESCRIPTION")
  endif()

  message(STATUS "Configuring \"${CONFIGURE_DEPENDENCY_TARGET}\"...")

  option(BUILD_${CONFIGURE_DEPENDENCY_TARGET} "${CONFIGURE_DEPENDENCY_DESCRIPTION}")
  # find out if we actually need to build the package ourselves
  if(NOT BUILD_${CONFIGURE_DEPENDENCY_TARGET})
    find_package(${CONFIGURE_DEPENDENCY_TARGET} QUIET)
    if(${CONFIGURE_DEPENDENCY_TARGET}_FOUND OR ${CONFIGURE_DEPENDENCY_PKG_VARNAME}_FOUND)
      message(STATUS "Package \"${CONFIGURE_DEPENDENCY_TARGET}\" has been found, using system supplied package.")
    else()
      message(WARNING "Package \"${CONFIGURE_DEPENDENCY_TARGET}\" was not found, building from external source.")
      set(BUILD_${CONFIGURE_DEPENDENCY_TARGET} ON CACHE BOOL "${CONFIGURE_DEPENDENCY_DESCRIPTION}" FORCE)
    endif()
  else()
    message(STATUS "Package \"${CONFIGURE_DEPENDENCY_TARGET}\" already configured to build from external source.")
  endif()

  # if we need (or want) to build the dependency ourselves, include the necessary external projects
  if(BUILD_${CONFIGURE_DEPENDENCY_TARGET})
    if(CONFIGURE_DEPENDENCY_SEPARATE_PLATFORMS)
      if(MSVC)
        include("dependencies/addExternalProject_${CONFIGURE_DEPENDENCY_TARGET}_win")
      else()
        include("dependencies/addExternalProject_${CONFIGURE_DEPENDENCY_TARGET}_linux")
      endif()
    else()
      include("dependencies/addExternalProject_${CONFIGURE_DEPENDENCY_TARGET}")
    endif()
  endif()
  
endmacro()

#------------------------------------------------------------------------------
# List of dependencies:

# glm
configure_dependency(TARGET glm DESCRIPTION "Build the OpenGL Mathematics library from source")

# libusb-1.0
configure_dependency(TARGET libusb-1.0 DESCRIPTION "Build libusb from source" PKG_VARNAME LIBUSB_1 SEPARATE_PLATFORMS)

# freenect
configure_dependency(TARGET libfreenect DESCRIPTION "Build libfreenect from source")

# eigen
configure_dependency(TARGET Eigen3 DESCRIPTION "Build Eigen3 from source")
