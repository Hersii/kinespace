#pragma once

#if defined(__cplusplus)
  // Code being compiled with C++ compiler 
  #define KINESPACE_EXTERN_C extern "C"
#else 
  // Code being compiled with C compiler (Not C++ compiler)
  #define KINESPACE_EXTERN_C 
#endif 

#if defined(_WIN32)
  // MS Windows DLLs (*.dll)
  #define KINESPACE_EXPORT_C KINESPACE_EXTERN_C __declspec(dllexport)
#else 
  // Unix-like Shared Object (.so) operating systems and GCC.
  #define KINESPACE_EXPORT_C KINESPACE_EXTERN_C __attribute__ ((visibility ("default")))
#endif 