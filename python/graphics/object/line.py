from typing import Tuple
from core.math.vertex import Point, Vertex
from graphics.object.drawable import DrawableDynamic2D, DrawableStatic3D
from OpenGL import GL

import numpy as np


class Line2D(DrawableDynamic2D):
    def __init__(self, points: Tuple[Point, Point] = None):
        if not points:
            points = np.zeros(2, dtype=Point._dtype)
        super().__init__(points, GL.GL_LINES)

class Line3D(DrawableStatic3D):
    def __init__(self, verts: Tuple[Vertex, Vertex] = None):
        if not verts:
            verts = np.zeros(2, dtype=Vertex._dtype)
        super().__init__(verts, GL.GL_LINES)
