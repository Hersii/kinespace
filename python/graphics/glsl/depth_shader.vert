#version 450

in vec3 pos;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

out gl_PerVertex
{
    vec4 gl_Position;
};
out float depth;

void main() {
  gl_Position = projection * view * model * vec4(pos, 1.0f);
  depth = length(pos);
}