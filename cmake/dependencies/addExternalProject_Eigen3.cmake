set(EP_EIGEN "eigen")
set(EP_EIGEN_TAG "3.3.9" CACHE STRING "eigen version (git tag)")

ExternalProject_Add("${EP_EIGEN}"
    GIT_REPOSITORY  "https://gitlab.com/libeigen/eigen.git"
    GIT_TAG         "${EP_EIGEN_TAG}"
    GIT_SHALLOW     ON

    CMAKE_CACHE_ARGS
        -DCMAKE_BUILD_TYPE:STRING=Release
        -DCMAKE_INSTALL_PREFIX:PATH=<INSTALL_DIR>
)
