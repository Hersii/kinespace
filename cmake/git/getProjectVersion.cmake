macro(set_version major minor patch tweak)
    set(PROJECT_VERSION_MAJOR ${major})
    set(PROJECT_VERSION_MINOR ${minor})
    set(PROJECT_VERSION_PATCH ${patch})
    set(PROJECT_VERSION_TWEAK ${tweak})
    string(JOIN "." PROJECT_VERSION 
        ${PROJECT_VERSION_MAJOR} 
        ${PROJECT_VERSION_MINOR} 
        ${PROJECT_VERSION_PATCH}
        ${PROJECT_VERSION_TWEAK})

    set(${PROJECT_NAME}_VERSION_MAJOR ${PROJECT_VERSION_MAJOR})
    set(${PROJECT_NAME}_VERSION_MINOR ${PROJECT_VERSION_MINOR})
    set(${PROJECT_NAME}_VERSION_PATCH ${PROJECT_VERSION_PATCH})
    set(${PROJECT_NAME}_VERSION_TWEAK ${PROJECT_VERSION_TWEAK})
    set(${PROJECT_NAME}_VERSION ${PROJECT_VERSION})
endmacro()

set_version(0 0 0 0)

include(FindGit)
find_package(Git)

if (NOT Git_FOUND)
    message(FATAL_ERROR "Git not found")
elseif(EXISTS "${CMAKE_CURRENT_SOURCE_DIR}/.git")
    # get project version
    execute_process(COMMAND ${GIT_EXECUTABLE} rev-parse --short HEAD
        WORKING_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}"
        OUTPUT_VARIABLE PACKAGE_GIT_VERSION
        ERROR_QUIET
        OUTPUT_STRIP_TRAILING_WHITESPACE)

    # TODO: parse version
    set_version(0 0 0 0)
endif()