from ctypes import c_void_p
import numpy as np
from core.math.vector.vector2 import Vector2
from pyrr import Vector3, Vector4

_Vertex = np.dtype(
    [
        ("pos", Vector3(dtype=np.float32).dtype, Vector3().shape),
        ("norm", Vector3(dtype=np.float32).dtype, Vector3().shape),
        ("col", Vector4(dtype=np.float32).dtype, Vector4().shape),
        ("tex", Vector2(dtype=np.float32).dtype, Vector2().shape),
    ]
)


class Vertex(np.ndarray):
    """Vertex object containing pos(v3), norm(v3), col(v4), tex(v2) coordinates."""
    _dtype = _Vertex

    # delete unsupported operators
    __add__ = None
    __sub__ = None
    __mul__ = None
    __div__ = None
    __truediv__ = None
    __mod__ = None
    __and__ = None
    __or__ = None
    __xor__ = None

    def __new__(cls, value=None):
        if value is not None:
            obj = np.array(value, dtype=cls._dtype, copy=True).view(cls)
        else:
            obj = np.array(0, dtype=cls._dtype).view(cls)

        if obj.ndim != 0:
            raise ValueError(f'"Vertex" is a scalar array type, cannot copy {type(obj)} with {obj.ndim} dimensions')

        return obj

    def getpos(self):
        """Get position vector.

        Returns:
            Vector3: position vector
        """
        return self["pos"].view(Vector3)

    def getnorm(self):
        """Get normal vector.

        Returns:
            Vector3: normal vector
        """
        return self["norm"].view(Vector3)

    def getcol(self):
        """Get vertex color.

        Returns:
            Vector4: vertex color
        """
        return self["col"].view(Vector4)

    def gettex(self):
        """Get texture coordinates.

        Returns:
            Vector2: texture coordinates
        """
        return self["tex"].view(Vector2)

    # defer operations to numpy
    def setpos(self, value):
        """Set position vector.

        Args:
            value (Vector3): position vector
        """
        self.pos[:] = value

    def setnorm(self, value):
        """Set normal vector.

        Args:
            value (Vector3): normal vector
        """
        self.norm[:] = value

    def setcol(self, value):
        """Set vertex color.

        Args:
            value (Vector4): vertex color
        """
        self.col[:] = value

    def settex(self, value):
        """Set texture coordinates

        Args:
            value (Vector2): texture coordinates
        """
        self.tex[:] = value

    pos = property(getpos, setpos)
    norm = property(getnorm, setnorm)
    col = property(getcol, setcol)
    tex = property(gettex, settex)

    @classmethod
    def get_offset(cls, attr: str):
        """Get attribute offset in vector.

        Args:
            attr (str): attribute name

        Returns:
            int: attribute offset
        """
        return c_void_p(cls._dtype.fields[attr][1])

    @classmethod
    def pos_offset(cls):
        """Get position value offset in vector.

        Returns:
            int: position value offset
        """
        return cls.get_offset("pos")

    @classmethod
    def norm_offset(cls):
        """Get normal value offset in vector.

        Returns:
            int: normal value offset
        """
        return cls.get_offset("norm")

    @classmethod
    def col_offset(cls):
        """Get color value offset in vector.

        Returns:
            int: color value offset
        """
        return cls.get_offset("col")

    @classmethod
    def tex_offset(cls):
        """Get texture value offset in vector.

        Returns:
            int: texture value offset
        """
        return cls.get_offset("tex")


_Point = np.dtype(
    [
        ("pos", Vector2(dtype=np.float32).dtype, Vector2().shape),
        ("col", Vector4(dtype=np.float32).dtype, Vector4().shape),
        ("tex", Vector2(dtype=np.float32).dtype, Vector2().shape),
    ]
)


class Point(np.ndarray):
    """Point object containing pos(v2), col(v4), tex(v2) coordinates."""    
    _dtype = _Point

    # delete unsupported operators
    __add__ = None
    __sub__ = None
    __mul__ = None
    __div__ = None
    __truediv__ = None
    __mod__ = None
    __and__ = None
    __or__ = None
    __xor__ = None

    def __new__(cls, value=None):
        if value is not None:
            obj = np.array(value, dtype=cls._dtype, copy=True).view(cls)
        else:
            obj = np.array(0, dtype=cls._dtype).view(cls)

        if obj.ndim != 0:
            raise ValueError(f'"Point" is a scalar array type, cannot copy {type(obj)} with {obj.ndim} dimensions')

        return obj

    def getpos(self):
        """Get position vector.

        Returns:
            Vector3: position vector
        """
        return self["pos"].view(Vector3)

    def getcol(self):
        """Get vertex color.

        Returns:
            Vector4: vertex color
        """
        return self["col"].view(Vector4)

    def gettex(self):
        """Get texture coordinates.

        Returns:
            Vector2: texture coordinates
        """
        return self["tex"].view(Vector2)

    # defer operations to numpy
    def setpos(self, value):
        """Set position vector.

        Args:
            value (Vector3): position vector
        """
        self.pos[:] = value

    def setcol(self, value):
        """Set vertex color.

        Args:
            value (Vector4): vertex color
        """
        self.col[:] = value

    def settex(self, value):
        """Set texture coordinates

        Args:
            value (Vector2): texture coordinates
        """
        self.tex[:] = value

    pos = property(getpos, setpos)
    col = property(getcol, setcol)
    tex = property(gettex, settex)

    @classmethod
    def get_offset(cls, attr: str):
        """Get attribute offset in vector.

        Args:
            attr (str): attribute name

        Returns:
            int: attribute offset
        """
        return c_void_p(cls._dtype.fields[attr][1])

    @classmethod
    def pos_offset(cls):
        """Get position value offset in vector.

        Returns:
            int: position value offset
        """
        return cls.get_offset("pos")

    @classmethod
    def col_offset(cls):
        """Get color value offset in vector.

        Returns:
            int: color value offset
        """
        return cls.get_offset("col")

    @classmethod
    def tex_offset(cls):
        """Get texture value offset in vector.

        Returns:
            int: texture value offset
        """
        return cls.get_offset("tex")
