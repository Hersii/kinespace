if(UNIX)
    option(INSTALL_USB_RULES "Install udev rules to machine for kinect" ON)

    if(INSTALL_USB_RULES)
        install(FILES ${CMAKE_CURRENT_SOURCE_DIR}/cmake/data/66-kinect.rules
            DESTINATION /etc/udev/rules.d)
        install(FILES ${CMAKE_CURRENT_SOURCE_DIR}/cmake/data/66-kinect-autosuspend.conf
            DESTINATION /etc/tlp.d)
    endif()
endif()
