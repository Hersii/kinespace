# KineSpace

3D Space Mapping and Computer Vision assistance for the Microsoft Kinect (v1)

Possible usages for example: Locating obstacles for drones, Architectural Mapping, Persistent memory of static objects

# Dependencies

To build you will need:

- [opencv](https://github.com/opencv/opencv)
    - [opencv_contrib](https://github.com/opencv/opencv_contrib)
    - [cuda toolkit](https://developer.nvidia.com/cuda-downloads)
- [libfreenect](https://github.com/OpenKinect/libfreenect)
    - [libusb](https://github.com/libusb/libusb/releases/tag/v1.0.23) >= 1.0.22
    - [CMake](https://cmake.org/download/) >= 3.1.0
    - [python](https://www.python.org/downloads/) >= 2.7 or >= 3.3

To build and run the examples, you will need:

- [OpenGL](https://www.khronos.org/opengl/wiki/Getting_Started#Downloading_OpenGL)
- [freeGLUT](http://freeglut.sourceforge.net/)
- [pthreads-win32](https://sourceforge.net/projects/pthreads4w/) for Windows
