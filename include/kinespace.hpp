#pragma once

#define API_VERSION_MAJOR 0
#define API_VERSION_MINOR 1

/** Macro EXPORT_CPP makes a symbol visible. */
#if defined(_WIN32)
  #define EXPORT_CPP __declspec(dllexport)
#else
  #define EXPORT_CPP __attribute__ ((visibility ("default")))
  // If not compiled on Windows, remove declspec compiler extension.
  #ifndef __declspec
    // For GCC only - make exported symbol visible symbol 
    #define __declspec(param) __attribute__ ((visibility ("default")))
  #endif
#endif 