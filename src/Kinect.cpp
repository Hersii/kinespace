#include <memory>
#include "Kinect.hpp"

std::shared_ptr<Freenect::Freenect> kinespace::freenect_inst = std::make_shared<Freenect::Freenect>();

kinespace::Kinect::Kinect(freenect_context *_ctx, int _index):
    Freenect::FreenectDevice(_ctx, _index)
{
    setDepthFormat(FREENECT_DEPTH_REGISTERED);
}

freenect_frame_mode kinespace::Kinect::getVideoFrameDescriptor()
{
    return freenect_find_video_mode(getVideoResolution(), getVideoFormat());
}

freenect_frame_mode kinespace::Kinect::getDepthFrameDescriptor()
{
    return freenect_find_depth_mode(getDepthResolution(), getDepthFormat());
}

void kinespace::Kinect::VideoCallback(void *video, uint32_t timestamp)
{
    PMutex::PScopedLock lock(video_mutex);
    uint8_t* rgb = static_cast<uint8_t*>(video);
    std::copy(rgb, rgb+getVideoBufferSize(), reinterpret_cast<uint8_t*>(video_buffer.begin()));
    video_cond_var.signal();
}

void kinespace::Kinect::DepthCallback(void *depth, uint32_t timestamp)
{
    PMutex::PScopedLock lock(depth_mutex);
    uint16_t* mm = static_cast<uint16_t*>(depth);
    std::copy(mm, mm+getDepthBufferSize(), depth_buffer.begin());
    depth_cond_var.signal();
}

void kinespace::Kinect::getVideo(std::array<glm::vec<3, uint8_t>, 640 * 480> &buf)
{
    PMutex::PScopedLock lock(video_mutex);
    if (video_cond_var.wait(video_mutex, 1000))
    {
        // LOG TIMEOUT WARNING
        return;
    }
    std::swap(buf, video_buffer);
}

void kinespace::Kinect::getDepth(std::array<uint16_t, 640 * 480> &buf)
{
    PMutex::PScopedLock lock(depth_mutex);
    if (video_cond_var.wait(video_mutex, 1000))
    {
        // LOG TIMEOUT WARNING
        return;
    }
    std::swap(buf, depth_buffer);
}
