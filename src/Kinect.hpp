#pragma once

#include <array>
#include "PMutex.hpp"
#include "glm/glm.hpp"
#include "libfreenect/libfreenect.hpp"

namespace kinespace
{
    extern std::shared_ptr<Freenect::Freenect> freenect_inst;

    class Kinect : public Freenect::FreenectDevice
    {
    private:
        PMutex video_mutex;
        PCondVar video_cond_var;
        PMutex depth_mutex;
        PCondVar depth_cond_var;
        std::array<glm::vec<3, uint8_t>, 640 * 480> video_buffer;
        std::array<uint16_t, 640 * 480> depth_buffer;

    public:
        Kinect(freenect_context *_ctx, int _index);

        freenect_frame_mode getVideoFrameDescriptor();
        freenect_frame_mode getDepthFrameDescriptor();

		// Do not call directly even in child
		virtual void VideoCallback(void *video, uint32_t timestamp);
		// Do not call directly even in child
		virtual void DepthCallback(void *depth, uint32_t timestamp);

        void getVideo(std::array<glm::vec<3, uint8_t>, 640 * 480> &buf);
        void getDepth(std::array<uint16_t, 640 * 480> &buf);
    protected:
        size_t getVideoBufferLength();
        size_t getDepthBufferLength();
    };
} // namespace kinespace
