import os.path as path

from core.configuration import GLSL_PATH
from core.math.vertex import Vertex
from graphics.object.camera import Camera
from graphics.object.line import Line3D
from graphics.object.pointcloud import KinectPointCloud
from graphics.renderer import Renderer
from graphics.shader import Shader
from OpenGL import GL
from PyQt5.QtCore import Qt, QThread
from PyQt5.QtGui import QMouseEvent, QWheelEvent
from PyQt5.QtWidgets import QApplication, QOpenGLWidget
from pyrr.objects.vector3 import Vector3
from utils.log_factory import create_class_logger


# TODO: WIP
class PointCloudViewer(QOpenGLWidget):
    def __init__(self, version_profile, *args, **kwargs):
        QOpenGLWidget.__init__(self, *args, **kwargs)
        self.logger = create_class_logger(PointCloudViewer)
        self.logger.debug("Initializing")

        self._gl = None
        self._version_profile = version_profile

        self.logger.debug("Set up rendering engine")
        self.renderer = Renderer()

        self.logger.debug("Create Viewer camera")
        self.camera = Camera(self.size().width(), self.size().height())
        self.camera.move_speed = 0.002
        self.camera.rotation_speed = 0.003
        self.camera.translate(Vector3((-0.3, -0.3, -1.5)))
        self.renderer.cameras.append(self.camera)
        self.renderer.change_camera(len(self.renderer.cameras) - 1)

        self.mouse_dragging = False
        self.mouse_prev_pos = (0, 0)

        self._init_axis_lines()

        self.point_cloud = KinectPointCloud()
        self.worker_thread = QThread()

        self.point_cloud.moveToThread(self.worker_thread)
        self.point_cloud.finished.connect(self.worker_thread.quit)
        self.worker_thread.started.connect(self.point_cloud.run)
        self.renderer.drawables.append(self.point_cloud)

        self.logger.info("Initialized")

    def _init_axis_lines(self):
        x_axis = Line3D(
            [
                Vertex(((-1, 0, 0), (1, 0, 0), (0.33, 0, 0, 1), (0, 0))),  # pos, norm, col, tex
                Vertex(((1, 0, 0), (1, 0, 0), (1, 0, 0, 1), (0, 0))),
            ]
        )
        y_axis = Line3D(
            [
                Vertex(((0, -1, 0), (0, 1, 0), (0, 0.33, 0, 1), (0, 0))),
                Vertex(((0, 1, 0), (0, 1, 0), (0, 1, 0, 1), (0, 0))),
            ]
        )
        z_axis = Line3D(
            [
                Vertex(((0, 0, -1), (0, 0, 1), (0, 0, 0.33, 1), (0, 0))),
                Vertex(((0, 0, 1), (0, 0, 1), (0, 0, 1, 1), (0, 0))),
            ]
        )
        self.renderer.drawables.append(x_axis)
        self.renderer.drawables.append(y_axis)
        self.renderer.drawables.append(z_axis)

    def mousePressEvent(self, event: QMouseEvent):
        self.mouse_dragging = True
        self.mouse_prev_pos = event.x(), event.y()

    def mouseReleaseEvent(self, event: QMouseEvent):
        self.mouse_dragging = False
        self.mouse_prev_pos = 0, 0

    def mouseMoveEvent(self, event: QMouseEvent):
        if event.buttons() == Qt.NoButton:
            return

        delta_mouse = self.mouse_prev_pos[0] - event.x(), self.mouse_prev_pos[1] - event.y()
        self.logger.debug("Mouse input: {}", delta_mouse)

        if event.buttons() == Qt.LeftButton:
            # "walk" view
            self.camera.walk(delta_mouse[1])
            self.camera.move(self.camera.transform.right_axis() * -delta_mouse[0])
            self.logger.debug("Camera pos: {}", self.camera.transform.get_translation())
            event.accept()

        if event.buttons() == Qt.MiddleButton:
            # translate view
            pan_vector = (
                self.camera.transform.right_axis() * -delta_mouse[0] + self.camera.transform.up_axis() * delta_mouse[1]
            )
            self.camera.move(pan_vector)
            self.logger.debug("Camera pos: {}", self.camera.transform.get_translation())
            event.accept()

        if event.buttons() == Qt.RightButton:
            # rotate view
            self.camera.rotate(delta_mouse[1], delta_mouse[0])
            self.logger.debug("Camera look vector: {}", self.camera.transform.forward_axis())
            event.accept()

        if self.mouse_dragging:
            self.mouse_prev_pos = event.x(), event.y()

    def wheelEvent(self, event: QWheelEvent):
        modifiers: Qt.KeyboardModifiers = QApplication.keyboardModifiers()
        if modifiers == Qt.ControlModifier:
            # rotation speed
            self.camera.rotation_speed += event.angleDelta().y() / 360
            event.accept()
        else:
            # translation speed
            self.camera.move_speed += event.angleDelta().y() / 360
            event.accept()

    def initializeGL(self):
        self.logger.debug("Set QtSurface format")
        # https://stackoverflow.com/a/33482388
        self._gl = self.context().versionFunctions(self._version_profile)
        self._gl.initializeOpenGLFunctions()

        # Initialize renderer
        depth_shaders = [
            Shader(GL.GL_VERTEX_SHADER, path.join(GLSL_PATH, "depth_shader.vert")),
            Shader(GL.GL_FRAGMENT_SHADER, path.join(GLSL_PATH, "depth_shader.frag")),
        ]
        color_shaders = [
            Shader(GL.GL_VERTEX_SHADER, path.join(GLSL_PATH, "color_shader.vert")),
            Shader(GL.GL_FRAGMENT_SHADER, path.join(GLSL_PATH, "color_shader.frag")),
        ]
        self.renderer.create_pipeline("depth", *depth_shaders)
        self.renderer.create_pipeline("color", *color_shaders)

        self.renderer.change_pipeline("color")
        self.renderer.init()

        self.worker_thread.start()

    def paintGL(self):
        self.renderer.draw()

        # Send update request to the Qt Framework so it calls paintGL() ASAP.
        # IMPROVEMENT: only update on data change or camera movement
        self.update()

    def resizeGL(self, w, h):
        self.renderer.resize(w, h)
