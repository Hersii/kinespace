import numpy as np
from OpenGL import GL


class Texture:
    def __init__(self):
        self.id = 0
        self.wrapping = []
        self.filtering = []
        self._texdata: np.ndarray
        self._setup()

    def update(self, texdata: np.ndarray):
        np.copyto(self._texdata, texdata)
        GL.glBindTexture(GL.GL_TEXTURE_2D, self.id)
        GL.glTexImage2D(
            GL.GL_TEXTURE_2D,
            0,
            GL.GL_RGB,
            self._texdata[0].size(),
            self._texdata.size(),
            0,
            GL.GL_RGB,
            GL.GL_UNSIGNED_BYTE,
            self._texdata,
        )
        GL.glGenerateMipmap(GL.GL_TEXTURE_2D)

    def _setup(self):
        self.id = GL.glGenTextures(1)
        GL.glBindTexture(GL.GL_TEXTURE_2D, self.id)
        for wrapsetting in self.wrapping:
            GL.glTexParameteri(GL.GL_TEXTURE_2D, wrapsetting, GL.GL_REPEAT)
        for filtersetting in self.filtering:
            GL.glTexParameteri(GL.GL_TEXTURE_2D, filtersetting, GL.GL_LINEAR)
