import random

import numpy as np
from core.kinect_device import KinectDevice
from core.math.vertex import Vertex
from graphics.object.drawable import DrawableDynamic3D
from OpenGL import GL
from PyQt5.QtCore import QObject, pyqtSignal, pyqtSlot


class PointCloudMeta(type(QObject), type(DrawableDynamic3D)):
    pass


#TODO: implement this
class PointCloud(DrawableDynamic3D, QObject, metaclass=PointCloudMeta):
    def __init__(self, resolution: tuple, parent=None):
        DrawableDynamic3D.__init__(self, np.zeros(resolution, dtype=Vertex._dtype), GL.GL_POINTS)
        QObject.__init__(self, parent)

    def _init_test_data(self, shape: tuple):
        self.depth_data = np.array(
            [[random.randint(0, 2 ** 11) for y in range(shape[1])] for x in range(shape[0])],
            dtype=np.float32,
        )
        self.depth_data = np.array(
            [[x, y, depth] for (x, y), depth in np.ndenumerate(self.depth_data)], dtype=np.float32
        )
        self.depth_data = self.depth_data / ((2 ** 11) - 1) * 4
        self.color_data = np.array(
            [
                [
                    [random.randint(0, 2 ** 16), random.randint(0, 2 ** 16), random.randint(0, 2 ** 16)]
                    for y in range(shape[1])
                ]
                for x in range(shape[0])
            ],
            dtype=np.float32,
        ) / ((2 ** 16) - 1)

_KINECT_VIDEO_SIZE = (640, 480)

class KinectPointCloud(PointCloud):
    def __init__(self, parent=None):
        super().__init__(_KINECT_VIDEO_SIZE, parent)

        self.depth_data = None
        self.color_data = None

        self.kinect = KinectDevice(self.update_depth, self.update_color)

    @pyqtSlot()
    def run(self):
        # TODO: get async code working
        # self.kinect_thread = Thread(target=self.kinect.run)
        # self.kinect_thread.start()

        self.kinect.sync_update_depth()
        self.kinect.sync_update_color()

        self.depth_data = self._convert_depth_to_xyz()
        self.color_data = self._convert_color_type()

        self.data_ready.emit(self.depth_data, self.color_data)

    data_ready = pyqtSignal(bool)

    def update_depth(self, dev, depth, timestamp):
        # depth comes in as shape (480, 640), we want (640, 480)
        self.depth_data = np.transpose(depth)

    def update_color(self, dev, color, timestamp):
        # depth comes in as shape (480, 640, 3), we want (640, 480, 3)
        self.color_data = np.transpose(color, axes=(1, 0, 2))

    def _convert_depth_to_xyz(self):
        return np.array([[x, y, depth] for (x, y), depth in np.ndenumerate(self.depth_data)], dtype=np.float32) / (
            2 ** 11
        )

    def _convert_color_type(self):
        return np.array(self.color_data, dtype=np.float32) / ((2 ** 16) - 1)
