from typing import Dict, List, Union

from OpenGL import GL
from graphics.exceptions import (
    InvalidReferenceError,
    ShaderCompilationFailure,
    ShaderProgramLinkFailure,
    ShaderProgramValidationFailure,
)
from utils.log_factory import LogBase

import numpy as np

SHADER_ATTR_ID: Dict[GL.GLuint, Dict[str, int]] = {
    GL.GL_VERTEX_SHADER: {"pos": 0, "norm": 1, "col": 2, "tex": 3},
    GL.GL_GEOMETRY_SHADER: {
        # TBD
    },
    GL.GL_FRAGMENT_SHADER: {
        # TBD
    },
    GL.GL_COMPUTE_SHADER: {
        # TBD
    },
}


def convert_stages_to_bitmask(stages: List[GL.GLuint]):
    mask = 0
    for shader_type in stages:
        mask |= {
            GL.GL_VERTEX_SHADER: GL.GL_VERTEX_SHADER_BIT,
            GL.GL_GEOMETRY_SHADER: GL.GL_GEOMETRY_SHADER_BIT,
            GL.GL_FRAGMENT_SHADER: GL.GL_FRAGMENT_SHADER_BIT,
            GL.GL_COMPUTE_SHADER: GL.GL_COMPUTE_SHADER_BIT,
        }.get(shader_type, 0)
    return mask


def convert_stage_bitmask_to_stages(stage_bitmask):
    stages = []
    if GL.GL_VERTEX_SHADER_BIT & stage_bitmask:
        stages.append(GL.GL_VERTEX_SHADER)
    if GL.GL_GEOMETRY_SHADER_BIT & stage_bitmask:
        stages.append(GL.GL_GEOMETRY_SHADER)
    if GL.GL_FRAGMENT_SHADER_BIT & stage_bitmask:
        stages.append(GL.GL_FRAGMENT_SHADER)
    if GL.GL_COMPUTE_SHADER_BIT & stage_bitmask:
        stages.append(GL.GL_COMPUTE_SHADER)
    return stages


class _ShaderSource:
    def __init__(self, path):
        self.path = path
        self.file = None
        self.text = ""
        self._has_been_read = False

    def __del__(self):
        self.close()

    def __eq__(self, o):
        if isinstance(o, str):
            return o == self.path
        elif isinstance(o, _ShaderSource):
            return o.text == self.text
        return False

    def __enter__(self):
        self.open()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.close()

    def __str__(self) -> str:
        return self.path

    def __repr__(self) -> str:
        return f'<path: "{self.path}", open: {not self.file.closed}>'

    def _read(self) -> str:
        if not self.file or self.file.closed:
            self.open()
        self._has_been_read = True
        return self.file.read()

    def open(self):
        if not self.file or self.file.closed:
            self.file = open(self.path)
            self._has_been_read = False

    def close(self):
        if self.file and not self.file.closed:
            self.file.close()
            self._has_been_read = False

    def read(self) -> str:
        if self._has_been_read:
            return self.text
        self.text = self._read()
        return self.text


class Shader(LogBase):
    _log_prefix = "(Shader {sid}) "
    _log_attr_kwargs = {"sid": "id"}

    def __init__(self, shader_type, shader_paths: Union[List[str], str] = None):
        super().__init__()
        self.id = 0
        self.type = shader_type
        self.sources: List[_ShaderSource] = []  # file paths to shader files

        self._init_shader()
        if shader_paths:
            self.set_source(shader_paths)

    def __del__(self):
        self._log_debug("Deleted")
        GL.glDeleteShader(self.id)

    def __str__(self):
        return "\n".join((src.text for src in self.sources))

    def __repr__(self):
        return "<{cls} id:{id} type:{type}>".format(cls=type(self).__name__, id=self.id, type=self.type)

    def __eq__(self, o):
        if isinstance(o, Shader):
            return o.id == self.id
        elif isinstance(o, int):
            return o == self.id
        return False

    def _init_shader(self):
        if self.id:
            self.release()

        self.id = GL.glCreateShader(self.type)
        self._log_info("Inititalized as {type}", type=self.type)

    def _set_sources(self, shader_texts: List[str]):
        for idx, shader_text in enumerate(shader_texts):
            self._log_debug("Source no. {:03d}:", idx)
            for lineno, line in enumerate(shader_text.splitlines()):
                self._log_debug("{:03d}|{}", lineno, line)

        GL.glShaderSource(self.id, shader_texts)

    def _check_shader_compile_status(self):
        self._log_debug("Checking compile status")
        if not GL.glGetShaderiv(self.id, GL.GL_COMPILE_STATUS):
            self._log_warning("Compilation failed")
            for line in GL.glGetShaderInfoLog(self.id).splitlines():
                self._log_warning("| {}", line)
            return False
        return True

    def set_source(self, shader_paths: Union[List[str], str]):
        if isinstance(shader_paths, str):
            shader_paths = [shader_paths]
        self._log_info("Setting sources from: {}", shader_paths)
        self.sources = [_ShaderSource(path) for path in shader_paths]
        self._set_sources([src.read() for src in self.sources])

    def append_source(self, shader_paths: Union[List[str], str]):
        if isinstance(shader_paths, str):
            shader_paths = [shader_paths]
        self._log_info("Appending sources: {}", shader_paths)
        self.sources += [_ShaderSource(path) for path in shader_paths]
        self._log_debug("Sources: {}", self.id, self.sources)
        self._set_sources([src.read() for src in self.sources])

    def compile(self):
        self._log_info("Compiling shader")
        GL.glCompileShader(self.id)

        if not self._check_shader_compile_status():
            raise ShaderCompilationFailure(self._log_error("Shader unusable, please reload"))

    def release(self):
        self._log_info("Releasing shader resource")
        GL.glDeleteShader(self.id)
        self.id = 0

        for source in self.sources:
            self._log_debug('Source file "{}" closed', source.path)
            source.close()

    def reload(self):
        self._log_info("Reloading shader")
        if self.id:
            self.release()

        self._init_shader()

        self._log_info("Reloading shader sources")
        for shader_src in self.sources:
            self._log_debug('Reading shader source "{}"', shader_src)
            with shader_src:
                shader_src.read()
        self._set_sources([src.text for src in self.sources])


class ShaderProgram(LogBase):
    _log_prefix = "(Program {pid}) "
    _log_attr_kwargs = {"pid": "id"}

    def __init__(self):
        super().__init__()
        self.id = GL.glCreateProgram()
        self.stages: Dict[GL.GLuint, List[Shader]] = dict()
        self.linked = False

        GL.glProgramParameteri(self.id, GL.GL_PROGRAM_SEPARABLE, GL.GL_TRUE)

        self._log_info("Initialized")

    def __del__(self):
        self._log_debug("Deleted")
        GL.glDeleteProgram(self.id)

    def __str__(self):
        return "Program {id}".format(id=self.id)

    def __repr__(self):
        return "<{cls} id:{id} linked:{link} stages:{stages}>".format(
            cls=type(self).__name__, id=self.id, link=self.linked, stages=self.stages
        )

    def _check_shader(self, shader: Shader):
        if not shader.id:
            InvalidReferenceError(self._log_error("'shader' is a released resource"))

    def _attach_shader(self, shader: Shader):
        self._check_shader(shader)
        self._log_debug("Attaching shader {}", shader.id)
        GL.glAttachShader(self.id, shader.id)
        for attr_name, attr_loc in SHADER_ATTR_ID[shader.type].items():
            self._log_debug("Binding {name} to location {loc}", name=attr_name, loc=attr_loc)
            GL.glBindAttribLocation(self.id, attr_loc, attr_name)

    def _detach_shader(self, shader: Shader):
        self._check_shader(shader)
        self._log_info("Detaching shader {}", shader.id)
        GL.glDetachShader(self.id, shader.id)

    def clear_shaders(self):
        self._log_info("Clearing shader stages")
        for stage, shaders in self.stages.items():
            self._log_debug("Clearing {} stage, shaders: {}", stage, shaders)
            for shader in shaders:
                self._detach_shader(shader)

    def set_shaders(self, *shaders: Shader):
        self._log_info("Setting shaders: {}", shaders)
        self.clear_shaders()
        for shader in shaders:
            self._attach_shader(shader)
            if shader.type not in self.stages:
                self.stages[shader.type] = []
            self.stages[shader.type].append(shader)

    def add_shaders(self, *shaders: Shader):
        self._log_info("Adding shaders: {}", shaders)
        for shader in shaders:
            self._attach_shader(shader)
            if shader.type not in self.stages:
                self.stages[shader.type] = []
            self.stages[shader.type].append(shader)

    def remove_shaders(self, *shaders: Shader):
        self._log_info("Removing shaders: {}", shaders)
        for shader in shaders:
            self._detach_shader(shader)
            self.stages[shader.type].remove(shader)
            if not self.stages[shader.type]:
                del self.stages[shader.type]

    def purge_unused_shaders(self):
        self._log_info("Purging unused shaders")
        for stage, shaders in self.stages:
            self._log_debug("Purging stage {}", stage)
            purge_list = []
            for idx, shader in enumerate(shaders):
                if not shader.id:
                    self._log_debug("Shader unreferenced at index {}, adding to purge list", idx)
                    purge_list.append(idx)
            for idx in purge_list:
                self._log_debug("Deleting shader at index {}", idx)
                del shaders[idx]

    def _get_info_log(self) -> str:
        return GL.glGetProgramInfoLog(self.id)

    def _check_link_status(self):
        self._log_debug("Checking linking status")
        if not GL.glGetProgramiv(self.id, GL.GL_LINK_STATUS):
            self.linked = False
            self._log_warning("Linking failed")
            for line in self._get_info_log().splitlines():
                self._log_warning("| {}", line)
        else:
            self.linked = True
            self._log_debug("Linking successful")
            for line in self._get_info_log().splitlines():
                self._log_debug("| {}", line)

        return self.linked

    def link(self):
        self._log_info("Linking")
        GL.glLinkProgram(self.id)
        self.clear_shaders()

        if not self._check_link_status():
            raise ShaderProgramLinkFailure(self._log_error("Program unlinked, please relink"))

    def _check_validate_status(self, verbose=False):
        self._log_debug("Checking validate status")

        if not GL.glGetProgramiv(self.id, GL.GL_VALIDATE_STATUS):
            self._log_warning("Validation failed")
            if verbose:
                for line in self._get_info_log().splitlines():
                    self._log_warning("| {}", line)
            return False

        self._log_debug("Validation successful")
        if verbose:
            for line in self._get_info_log().splitlines():
                self._log_debug("| {}", line)
        return True

    def validate(self):
        self._log_debug("Validating")
        GL.glValidateProgram(self.id)

        if not self._check_validate_status(True):
            raise ShaderProgramValidationFailure(self._log_error("Validation failed, please check program log"))

    def use(self):
        if not self.linked:
            raise ShaderProgramLinkFailure(self._log_error("Program unlinked, please relink"))

        if not self._check_validate_status():
            raise ShaderProgramValidationFailure(self._log_error("Program invalid, can't execute"))

        GL.glUseProgram(self.id)
        self._log_info("Now in use for rendering")

    def get_stage_list(self):
        return list(self.stages.keys())

    def get_stage_bitmask(self):
        return convert_stages_to_bitmask(self.stages.keys())

    def get_uniform_location(self, uniform_name):
        return GL.glGetUniformLocation(self.id, uniform_name)

    @staticmethod
    def _check_uniform_type(value_type):
        if isinstance(value_type, type):
            if value_type not in (GL.GLfloat, GL.GLint, GL.GLuint):
                raise ValueError("uniform value type must be one of the following: (GL.GLfloat, GL.GLint, GL.GLuint)")
        elif isinstance(value_type, str):
            if value_type not in ("float", "int", "uint"):
                raise ValueError('uniform value type must be one of the following: ("float", "int", "uint")')

    @staticmethod
    def _convert_uniform_type_to_signature(value_type):
        ShaderProgram._check_uniform_type(value_type)
        if value_type == GL.GLfloat or value_type == "float":
            return "f"
        elif value_type == GL.GLint or value_type == "int":
            return "i"
        elif value_type == GL.GLuint or value_type == "uint":
            return "ui"

    def set_uniform(self, uniform_name, value_type, *uniform_values):
        uniform_loc = self.get_uniform_location(uniform_name)
        uniform_size = len(uniform_values)

        if uniform_size < 1 or uniform_size > 4:
            raise ValueError("set_uniform: uniform_size must be within 1 and 4 values")

        gl_pogram_uniform_func = getattr(
            GL, f"glProgramUniform{uniform_size}{ShaderProgram._convert_uniform_type_to_signature(value_type)}"
        )
        gl_pogram_uniform_func(self.id, uniform_loc, *uniform_values)

    def set_uniform_array(self, uniform_name, value_type, uniform_arrays: np.ndarray):
        uniform_loc = self.get_uniform_location(uniform_name)
        uniform_size = uniform_arrays.shape[1] if uniform_arrays.ndim == 2 else 1
        uniform_count = uniform_arrays.shape[0]

        if uniform_size < 1 or uniform_size > 4:
            raise ValueError("set_uniform_array: uniform_size must be within 1 and 4 values")

        gl_pogram_uniform_func = getattr(
            GL, f"glProgramUniform{uniform_size}{ShaderProgram._convert_uniform_type_to_signature(value_type)}v"
        )
        gl_pogram_uniform_func(self.id, uniform_loc, uniform_count, uniform_arrays)

    def set_uniform_matrix(self, uniform_name, uniform_matrices: np.ndarray, transpose=GL.GL_FALSE):
        uniform_loc = self.get_uniform_location(uniform_name)
        uniform_size = uniform_matrices.shape[1:] if uniform_matrices.ndim == 3 else uniform_matrices.shape
        uniform_count = uniform_matrices.shape[0] if uniform_matrices.ndim == 3 else 1

        if uniform_matrices.ndim < 2 or uniform_matrices.ndim > 3:
            raise ValueError("set_uniform_matrix: only 2 dimensional matrices (or matrix lists) are supported")

        if uniform_size not in ((2, 2), (3, 3), (4, 4), (2, 3), (3, 2), (2, 4), (4, 2), (3, 4), (4, 3)):
            raise ValueError(
                "set_uniform_matrix: uniform_size must be one of: "
                "(2, 2), (3, 3), (4, 4), (2, 3), (3, 2), (2, 4), (4, 2), (3, 4), (4, 3)"
            )

        def _convert_matrix_shape_to_signature(item_shape):
            if item_shape in ((2, 2), (3, 3), (4, 4)):
                return str(item_shape[0])
            else:
                return "x".join(item_shape)

        gl_pogram_uniform_func = getattr(
            GL,
            "glProgramUniformMatrix"
            f"{_convert_matrix_shape_to_signature(uniform_size)}"
            f"{ShaderProgram._convert_uniform_type_to_signature(GL.GLfloat)}"
            "v",
        )
        gl_pogram_uniform_func(self.id, uniform_loc, uniform_count, transpose, uniform_matrices)


# https://www.khronos.org/opengl/wiki/Shader_Compilation#Program_pipelines
class ShaderPipeline(LogBase):
    _log_prefix = "(Pipeline {spid}) "
    _log_attr_kwargs = {"spid": "id"}

    def __init__(self):
        super().__init__()
        _id = np.empty(1, dtype=np.uint32)
        GL.glGenProgramPipelines(1, _id)
        self.id = _id[0]
        self.stages: Dict[GL.GLuint, ShaderProgram] = {}
        self._log_info("Initialized")

    def __del__(self):
        self._log_info("Deleted")
        GL.glDeleteProgramPipelines(1, np.asarray([self.id]))

    def use_shader_stages(self, program: ShaderProgram, stages: List[GL.GLuint] = None):
        if not stages:
            stages = list(program.stages.keys())

        stage_mask = convert_stages_to_bitmask(stages)

        overwrite_stage_mask = stage_mask & convert_stages_to_bitmask(list(self.stages.keys()))
        if overwrite_stage_mask:
            self._log_info(
                "Overwriting already loaded pipeline stages: {}",
                convert_stage_bitmask_to_stages(overwrite_stage_mask),
            )

        self._log_info("Using {} for stages {}", program, stages)
        GL.glUseProgramStages(self.id, stage_mask, program.id)
        for stage in stages:
            self.stages[stage] = program

    def remove_stages(self, stages: List[GL.GLuint]):
        stage_mask = convert_stages_to_bitmask(stages)

        self._log_info("Removing stages {}", stages)
        GL.glUseProgramStages(self.id, stage_mask, 0)
        for stage in stages:
            del self.stages[stage]

    def clear_stages(self):
        self._log_info("Clearing all stages")
        GL.glUseProgramStages(self.id, GL.GL_ALL_SHADER_BITS, 0)
        self.stages.clear()

    def get_program(self, stage: GL.GLuint):
        return self.stages[stage]

    def bind(self):
        self._log_debug("Unbinding currently in use shader program from rendering")
        GL.glUseProgram(0)
        GL.glBindProgramPipeline(self.id)
        self._log_info("Now in use for rendering")
