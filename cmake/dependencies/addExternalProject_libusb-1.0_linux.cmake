set(EP_LIBUSB "libusb")
set(EP_LIBUSB_TAG "v1.0.24" CACHE STRING "libusb version (git tag)")
get_directory_property(EP_LIBUSB_PREFIX EP_PREFIX)
set(EP_LIBUSB_SRCDIR "${CMAKE_CURRENT_BINARY_DIR}/${EP_LIBUSB_PREFIX}/src/${EP_LIBUSB}")
set(EP_LIBUSB_BINDIR "${CMAKE_CURRENT_BINARY_DIR}/${EP_LIBUSB_PREFIX}")

ExternalProject_Add("${EP_LIBUSB}"
    GIT_REPOSITORY      "https://github.com/libusb/libusb.git"
    GIT_TAG             "${EP_LIBUSB_TAG}"
    GIT_SHALLOW         ON

    BINARY_DIR          "${EP_LIBUSB_SRCDIR}"

    CONFIGURE_COMMAND   "<SOURCE_DIR>/bootstrap.sh"
    COMMAND             "<SOURCE_DIR>/configure" --prefix=<INSTALL_DIR>
    BUILD_COMMAND       "${MAKE}"
)

set(LIBUSB_1_INCLUDE_DIR "${EP_LIBUSB_BINDIR}/include/libusb-1.0" CACHE PATH "libusb-1.0 include path" FORCE)
set(LIBUSB_1_LIBRARY "${EP_LIBUSB_BINDIR}/lib/libusb-1.0.so" CACHE FILEPATH "libusb-1.0 library" FORCE)
