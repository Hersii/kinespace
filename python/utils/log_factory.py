import logging
from inspect import getfullargspec
from typing import Dict, List


class BraceStyleMessage:
    def __init__(self, fmt, /, *args, **kwargs):
        self.fmt = fmt
        self.args = args
        self.kwargs = kwargs

    def __str__(self):
        return str(self.fmt).format(*self.args, **self.kwargs)


class BraceStyleAdapter(logging.LoggerAdapter):
    """Brace ("new") style adapter for Logger

    Note: The msg format string cannot contain keys related to Logger.log
    method arguments, like: `level`, `msg`, `args`, `exc_info`, `extra` or `stack_info`
    """

    def __init__(self, logger: logging.Logger, extra=None):
        super().__init__(logger, extra or {})
        self.stacklevel = 1

    def process(self, msg, kwargs):
        log_kwargs = {'stacklevel': self.stacklevel}
        for log_kwargs_key in getfullargspec(self.logger._log).args[1:]:
            if "{" + log_kwargs_key + "}" in msg:
                raise KeyError('Illegal format string, contains "{}"'.format(log_kwargs_key))
            if log_kwargs_key in kwargs:
                log_kwargs[log_kwargs_key] = kwargs[log_kwargs_key]
                del kwargs[log_kwargs_key]
        return msg, log_kwargs

    def format(self, msg: str, /, *args, **kwargs) -> str:
        self.process(msg, kwargs) # remove illegal kwargs for str.format()
        return msg.format(*args, **kwargs)

    def log(self, level, msg, /, *args, **kwargs):
        if self.isEnabledFor(level):
            msg, log_kwargs = self.process(msg, kwargs)
            self.logger._log(level, BraceStyleMessage(msg, *args, **kwargs), (), **log_kwargs)


def create_class_logger(cls: type) -> BraceStyleAdapter:
    """Create Brace ("new") style Logger for class `cls`

    Parameters
    ----------
    - cls : type
        - Initializes Logger with the class' module and qualified name

    Returns
    -------
    - Type: BraceStyleAdapter
        - Logger that takes "new"-style format strings
    """
    return BraceStyleAdapter(logging.getLogger(".".join((cls.__module__, cls.__qualname__))))


class LogBase:
    _log_prefix = ""
    _log_attr_args: List[str] = []
    _log_attr_kwargs: Dict[str, str] = {}

    @classmethod
    def _log_set_prefix(cls, fmt: str):
        cls._log_prefix = fmt

    @classmethod
    def _log_set_attributes(cls, *fmt_attr_args, **fmt_attr_kwargs):
        cls._log_attr_args = fmt_attr_args
        cls._log_attr_kwargs = fmt_attr_kwargs

    def __init__(self):
        self.logger = create_class_logger(type(self))
        self.logger.stacklevel = 3

    def _log_format_prefix(self):
        error_fmt = "Log prefix {ex_type}, {ex_msg}"
        try:
            return self._log_prefix.format(
                *(getattr(self, attr_name) for attr_name in self._log_attr_args),
                **{fmt_key: getattr(self, attr_name) for fmt_key, attr_name in self._log_attr_kwargs.items()},
            )
        except (AttributeError, IndexError) as ex:
            self.logger.warning(error_fmt, ex_type=type(ex).__name__, ex_msg=str(ex))
        except KeyError as ex:
            ex_msg = "Key {key} not found in {cls}".format(key=ex, cls=type(self).__name__)
            self.logger.warning(error_fmt, ex_type="KeyError", ex_msg=ex_msg)
        self.logger.debug("_log_prefix: {}", self._log_prefix)
        self.logger.debug("_log_attr_args: {}", self._log_attr_args)
        self.logger.debug("_log_attr_kwargs: {}", self._log_attr_kwargs)
        return ""

    def _log_format(self, fmt: str, /, *args, **kwargs) -> str:
        return self._log_format_prefix() + fmt.format(*args, **kwargs)

    def _log_debug(self, msg, /, *args, **kwargs):
        self.logger.debug(self._log_format_prefix() + msg, *args, **kwargs)
        return self.logger.format(msg, *args, **kwargs)

    def _log_info(self, msg, /, *args, **kwargs):
        self.logger.info(self._log_format_prefix() + msg, *args, **kwargs)
        return self.logger.format(msg, *args, **kwargs)

    def _log_warning(self, msg, /, *args, **kwargs):
        self.logger.warning(self._log_format_prefix() + msg, *args, **kwargs)
        return self.logger.format(msg, *args, **kwargs)

    def _log_error(self, msg, /, *args, **kwargs):
        self.logger.error(self._log_format_prefix() + msg, *args, **kwargs)
        return self.logger.format(msg, *args, **kwargs)

    def _log_critical(self, msg, /, *args, **kwargs):
        self.logger.critical(self._log_format_prefix() + msg, *args, **kwargs)
        return self.logger.format(msg, *args, **kwargs)

    def _log_exception(self, msg, /, *args, **kwargs):
        self.logger.exception(self._log_format_prefix() + msg, *args, **kwargs)
        return self.logger.format(msg, *args, **kwargs)
