#pragma once

#include <array>
#include "glm/glm.hpp"

namespace kinespace
{
    struct Point
    {
        glm::vec3 pos;
        glm::vec3 col;
    };

    class PointCloud
    {
    public:
        PointCloud();

        void update(std::array<Point, 640 * 480> pointbuf);

    private:
        std::array<Point, 640 * 480> points;
        glm::mat4x4 transformation_matx;
    };
} // namespace kinespace
