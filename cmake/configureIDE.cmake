option(CONF_IDE_FORCE_INIT "Force IDE config generation")

option(CONF_IDE_VSCODE "Configure VSCode")

if(CONF_IDE_VSCODE)
    # configure vscode workspace
    include("IDE/vscode/configureVsCode")    
endif()
