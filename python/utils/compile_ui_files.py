import os
import sys
import argparse
import traceback

from gui.configuration import uic


def __compile_ui_dir(_in, _out=None, **compile_ui_kwargs):
    def __compile_ui(ui_dir, ui_file):
        if ui_file.endswith('.ui'):
            py_dir = _out if _out else ui_dir
            py_file = ui_file[:-3] + '.py'

            # Make sure the destination directory exists.
            os.makedirs(py_dir, exist_ok=True)

            ui_path = os.path.join(ui_dir, ui_file)
            py_path = os.path.join(py_dir, py_file)

            ui_file = open(ui_path, 'r')
            py_file = open(py_path, 'w')

            try:
                uic.compileUi(ui_file, py_file, **compile_ui_kwargs)
            finally:
                ui_file.close()
                py_file.close()

    for dirpath, _, filenames in os.walk(_in):
        for uiname in filenames:
            __compile_ui(dirpath, uiname)


def main():
    argument_parser = argparse.ArgumentParser(os.path.basename(__file__))
    argument_parser.add_argument('input')
    argument_parser.add_argument('-o', '--out', default=None)
    argument_parser.add_argument('-d', '--directory', action='store_true')
    argument_parser.add_argument('--executable', action='store_true')
    parsed_args = argument_parser.parse_args()

    if parsed_args.directory:
        __compile_ui_dir(
            parsed_args.input, parsed_args.out,
            execute=parsed_args.executable
        )
    else:
        uic.compileUi(
            parsed_args.input, parsed_args.out,
            execute=parsed_args.executable
        )


if __name__ == "__main__":
    try:
        main()
    except Exception as ex:
        for frame in traceback.format_exception(type(ex), ex, None):
            print(frame, file=sys.stderr, end="")
        sys.exit(ex)
