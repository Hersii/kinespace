import os
import sys

try:
    from PyQt5.QtCore import Qt
    from PyQt5.QtWidgets import QMainWindow
except ImportError:
    print("PyQt5 must be installed to run this program.")
    sys.exit(1)

from gui.configuration import DESIGN_PATH, uic
from gui.widgets.PointCloudViewer import PointCloudViewer
from gui.windows.log_window import LogWindow


def _init_window(window, version_profile):
    window.setupUi(window)
    # replace the viewer with an implementation
    item = window.gridLayout.takeAt(0)
    item.widget().deleteLater()
    window.pointCloudViewer = PointCloudViewer(version_profile, window.centralWidget())
    window.pointCloudViewer.setObjectName("pointCloudViewer")
    window.gridLayout.addWidget(window.pointCloudViewer, 0, 0, 1, 1)


def load(version_profile = None):
    try:
        from gui.design.main import Ui_MainWindow
    except ImportError:
        print('warning: main.ui is not compiled, dynamically loading ui file...')
        main_window = uic.loadUi(os.path.join(DESIGN_PATH, 'main.ui'), baseinstance=QMainWindow)
        _init_window(main_window, version_profile)
    else:
        class MainWindow(QMainWindow, Ui_MainWindow):
            def __init__(self, parent=None, flags=Qt.WindowFlags()):
                super().__init__(parent=parent, flags=flags)
                _init_window(self, version_profile)

        main_window = MainWindow()

    log_window = LogWindow(main_window)
    main_window.actionLog_window.triggered.connect(log_window.show)

    return main_window, log_window
