from typing import Dict, List

from OpenGL import GL

from graphics.object.camera import Camera
from graphics.object.drawable import Drawable
from graphics.shader import Shader, ShaderPipeline, ShaderProgram
from utils.log_factory import LogBase
from utils.singleton import Singleton


class Renderer(LogBase, metaclass=Singleton):
    def __init__(self):
        super().__init__()
        self._log_debug("Renderer object initializing")

        self.drawables: List[Drawable] = []

        self._cur_camera = 0
        self.cameras: List[Camera] = [Camera(800, 600)]
        self._log_debug(
            "Creating default camera {}, index {}", self.cameras[self._cur_camera].viewport_dim, self._cur_camera
        )

        self._cur_pipeline = None
        self.pipelines: Dict[str, ShaderPipeline] = {}

        self._log_info("Renderer object initialized")

    def _assert_new_pipeline(self, pipeline_name):
        if pipeline_name in self.pipelines:
            raise ValueError(self._log_error('Pipeline "{}" already exists', pipeline_name))

    def create_pipeline(self, pipeline_name, *shaders: Shader):
        self._log_info('Creating pipeline "{}"', pipeline_name)
        self._assert_new_pipeline(pipeline_name)

        pipeline = ShaderPipeline()

        shader_stages: Dict[GL.GLuint, Shader] = {
            stage: [shader for shader in shaders if shader.type == stage]
            for stage in [GL.GL_VERTEX_SHADER, GL.GL_GEOMETRY_SHADER, GL.GL_FRAGMENT_SHADER, GL.GL_COMPUTE_SHADER]
        }
        for stage, shaders in shader_stages.items():
            if not shaders:
                continue
            self._log_debug("Stage {} shaders: {}", stage, shaders)

            self._log_debug("Creating ShaderProgram for stage {}", stage)
            program = ShaderProgram()
            program.add_shaders(*shaders)
            program.link()
            pipeline.use_shader_stages(program, [stage])

        self.pipelines[pipeline_name] = pipeline

    def add_pipeline(self, pipeline_name, pipeline: ShaderPipeline):
        self._log_info("Adding pipeline {}", pipeline_name)
        self._assert_new_pipeline(pipeline_name)
        self.pipelines[pipeline_name] = pipeline

    def change_pipeline(self, pipeline_name):
        self._log_info('Changing rendering pipeline to "{}"', pipeline_name)
        if pipeline_name not in self.pipelines:
            self._log_error('Pipeline "{}" does not exist', pipeline_name)
            return

        self._cur_pipeline = pipeline_name

    def change_camera(self, index):
        try:
            self.cameras[index]
        except IndexError:
            self._log_error("Camera change error, Camera index {} does not exist", index)
            return

        self.cameras[index].resize(*self.cameras[self._cur_camera].viewport_dim)
        self._cur_camera = index
        self._log_info("Camera changed to Camera index {}", index)

    def resize(self, window_width, window_height):
        self._log_info("Resizing to {}x{}", window_width, window_height)
        self.cameras[self._cur_camera].resize(window_width, window_height)
        vert_shader = self.pipelines[self._cur_pipeline].get_program(GL.GL_VERTEX_SHADER)
        self.cameras[self._cur_camera].update_projection(vert_shader)

    def init(self):
        self._log_debug("Begin setup of OpenGL")
        # Set some default values for the window
        GL.glClearColor(0, 0, 0.1, 0)
        GL.glEnable(GL.GL_DEPTH_TEST)
        GL.glEnable(GL.GL_PROGRAM_POINT_SIZE)
        GL.glPointSize(10)

        self._log_debug("Binding currently selected pipeline {}", self._cur_pipeline)
        pipeline = self.pipelines[self._cur_pipeline]
        pipeline.bind()

        self._log_debug("Setting up cameras")
        vert_shader = pipeline.get_program(GL.GL_VERTEX_SHADER)
        for camera in self.cameras:
            camera.setup(vert_shader)

        self._log_debug("Setting up drawable objects")
        for drawable in self.drawables:
            drawable.setup()

        self._log_info("Renderer initialized")

    def draw(self):
        # Clear viewport
        GL.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT)

        pipeline = self.pipelines[self._cur_pipeline]
        vert_shader = pipeline.get_program(GL.GL_VERTEX_SHADER)

        self.cameras[self._cur_camera].update(vert_shader)

        for drawable in self.drawables:
            drawable.update(vert_shader)
            drawable.draw()
