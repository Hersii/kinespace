option(CONF_IDE_VSCODE "Create configuration files for Visual Studio Code")

if(CONF_IDE_VSCODE)
    # configure .env
    include("IDE/vscode/configure.env")

    if(NOT EXISTS "${CMAKE_CURRENT_SOURCE_DIR}/.vscode" OR CONF_IDE_FORCE_INIT)
        message(STATUS "Creating Visual Studio Code configuration")

        file(MAKE_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}/.vscode")

        configure_file("${CMAKE_CURRENT_SOURCE_DIR}/cmake/templates/vscode/settings.json.in"
                    "${CMAKE_CURRENT_SOURCE_DIR}/.vscode/settings.json")

        file(COPY "${CMAKE_CURRENT_SOURCE_DIR}/cmake/data/.vscode/launch.json"
            DESTINATION "${CMAKE_CURRENT_SOURCE_DIR}/.vscode")
    else()
        message("${CMAKE_CURRENT_SOURCE_DIR}/.vscode already exists, skipping IDE config initialization")
    endif()

endif()
