from abc import abstractmethod

import numpy as np
from core.math.vertex import Point, Vertex
from graphics.object.sceneobject import SceneObject
from OpenGL import GL

from graphics.shader import SHADER_ATTR_ID, ShaderProgram


class Drawable(SceneObject):
    def __init__(self, mode=GL.GL_TRIANGLES):
        super().__init__()
        self._mode = mode
        self._vao: GL.GLuint = 0
        self._vbo: GL.GLuint = 0

    def __del__(self):
        if self._vao and self._vbo:
            GL.glDeleteVertexArrays(1, np.asarray([self._vao]))
            GL.glDeleteBuffers(1, np.asarray([self._vbo]))

    def _setup(self):
        vao = np.empty(1, dtype=np.uint32)
        vbo = np.empty(1, dtype=np.uint32)
        GL.glGenVertexArrays(1, vao)
        GL.glGenBuffers(1, vbo)
        self._vao = vao[0]
        self._vbo = vbo[0]

    def setup(self):
        self._setup()

    @abstractmethod
    def draw(self):
        raise NotImplementedError("There is no implementation for draw().")

    def update(self, vert_shader: ShaderProgram, verbose=False):
        if verbose:
            self.logger.debug("Update uniform model matrix \n{}", self.transform)
        vert_shader.set_uniform_matrix("model", self.transform)


class Drawable2D(Drawable):
    def __init__(self, points: np.ndarray, mode=GL.GL_TRIANGLES):
        super().__init__(mode)
        self._points: np.ndarray = np.array(points, dtype=Point._dtype)


class Drawable3D(Drawable):
    def __init__(self, vertices: np.ndarray, mode=GL.GL_TRIANGLES):
        super().__init__(mode)
        self._vertices: np.ndarray = np.array(vertices, dtype=Vertex._dtype)


class DrawableStatic2D(Drawable2D):
    def setup(self):
        super().setup()

        GL.glBindVertexArray(self._vao)
        GL.glBindBuffer(GL.GL_ARRAY_BUFFER, self._vbo)
        GL.glBufferData(GL.GL_ARRAY_BUFFER, self._points, GL.GL_STATIC_DRAW)

        # vertex position
        pos_attr = SHADER_ATTR_ID[GL.GL_VERTEX_SHADER]["pos"]
        GL.glEnableVertexAttribArray(pos_attr)
        GL.glVertexAttribPointer(pos_attr, 2, GL.GL_FLOAT, GL.GL_FALSE, self._points.itemsize, Point.pos_offset())

        # color
        col_attr = SHADER_ATTR_ID[GL.GL_VERTEX_SHADER]["pos"]
        GL.glEnableVertexAttribArray(col_attr)
        GL.glVertexAttribPointer(col_attr, 4, GL.GL_FLOAT, GL.GL_FALSE, self._points.itemsize, Point.col_offset())

        # vertex texture coords
        tex_attr = SHADER_ATTR_ID[GL.GL_VERTEX_SHADER]["tex"]
        GL.glEnableVertexAttribArray(tex_attr)
        GL.glVertexAttribPointer(tex_attr, 2, GL.GL_FLOAT, GL.GL_FALSE, self._points.itemsize, Point.tex_offset())

        GL.glBindVertexArray(0)

    def draw(self):
        GL.glBindVertexArray(self._vao)
        GL.glDrawArrays(self._mode, 0, len(self._points))
        GL.glBindVertexArray(0)


class DrawableDynamic2D(Drawable2D):
    def update_points(self, points: np.ndarray):
        np.copyto(self._points, points, casting="no")

    def setup(self):
        super().setup()

        GL.glBindVertexArray(self._vao)
        GL.glBindBuffer(GL.GL_ARRAY_BUFFER, self._vbo)
        GL.glBindVertexArray(0)

    def draw(self):
        GL.glBindVertexArray(self._vao)

        GL.glBindBuffer(GL.GL_ARRAY_BUFFER, self._vbo)
        GL.glBufferData(GL.GL_ARRAY_BUFFER, self._points, GL.GL_STATIC_DRAW)

        # vertex position
        pos_attr = SHADER_ATTR_ID[GL.GL_VERTEX_SHADER]["pos"]
        GL.glEnableVertexAttribArray(pos_attr)
        GL.glVertexAttribPointer(pos_attr, 2, GL.GL_FLOAT, GL.GL_FALSE, self._points.itemsize, Point.pos_offset())

        # color
        col_attr = SHADER_ATTR_ID[GL.GL_VERTEX_SHADER]["pos"]
        GL.glEnableVertexAttribArray(col_attr)
        GL.glVertexAttribPointer(col_attr, 4, GL.GL_FLOAT, GL.GL_FALSE, self._points.itemsize, Point.col_offset())

        # vertex texture coords
        tex_attr = SHADER_ATTR_ID[GL.GL_VERTEX_SHADER]["tex"]
        GL.glEnableVertexAttribArray(tex_attr)
        GL.glVertexAttribPointer(tex_attr, 2, GL.GL_FLOAT, GL.GL_FALSE, self._points.itemsize, Point.tex_offset())

        GL.glDrawArrays(self._mode, 0, len(self._points))

        GL.glBindVertexArray(0)


class DrawableStatic3D(Drawable3D):
    def setup(self):
        super().setup()

        GL.glBindVertexArray(self._vao)
        GL.glBindBuffer(GL.GL_ARRAY_BUFFER, self._vbo)
        GL.glBufferData(GL.GL_ARRAY_BUFFER, self._vertices, GL.GL_STATIC_DRAW)

        # vertex position
        pos_attr = SHADER_ATTR_ID[GL.GL_VERTEX_SHADER]["pos"]
        GL.glEnableVertexAttribArray(pos_attr)
        GL.glVertexAttribPointer(pos_attr, 3, GL.GL_FLOAT, GL.GL_FALSE, self._vertices.itemsize, Vertex.pos_offset())

        # vertex normals
        norm_attr = SHADER_ATTR_ID[GL.GL_VERTEX_SHADER]["norm"]
        GL.glEnableVertexAttribArray(norm_attr)
        GL.glVertexAttribPointer(norm_attr, 3, GL.GL_FLOAT, GL.GL_FALSE, self._vertices.itemsize, Vertex.norm_offset())

        # color
        col_attr = SHADER_ATTR_ID[GL.GL_VERTEX_SHADER]["col"]
        GL.glEnableVertexAttribArray(col_attr)
        GL.glVertexAttribPointer(col_attr, 4, GL.GL_FLOAT, GL.GL_FALSE, self._vertices.itemsize, Vertex.col_offset())

        # vertex texture coords
        tex_attr = SHADER_ATTR_ID[GL.GL_VERTEX_SHADER]["tex"]
        GL.glEnableVertexAttribArray(tex_attr)
        GL.glVertexAttribPointer(tex_attr, 2, GL.GL_FLOAT, GL.GL_FALSE, self._vertices.itemsize, Vertex.tex_offset())

        GL.glBindVertexArray(0)

    def draw(self):
        GL.glBindVertexArray(self._vao)
        GL.glDrawArrays(self._mode, 0, len(self._vertices))
        GL.glBindVertexArray(0)


class DrawableDynamic3D(Drawable3D):
    def update_vertices(self, vertices: np.ndarray):
        np.copyto(self._vertices, vertices, casting="no")

    def setup(self):
        super().setup()

        GL.glBindVertexArray(self._vao)
        GL.glBindBuffer(GL.GL_ARRAY_BUFFER, self._vbo)
        GL.glBindVertexArray(0)

    def draw(self):
        GL.glBindVertexArray(self._vao)

        GL.glBindBuffer(GL.GL_ARRAY_BUFFER, self._vbo)
        GL.glBufferData(GL.GL_ARRAY_BUFFER, self._vertices, GL.GL_DYNAMIC_DRAW)

        # vertex position
        pos_attr = SHADER_ATTR_ID[GL.GL_VERTEX_SHADER]["pos"]
        GL.glEnableVertexAttribArray(pos_attr)
        GL.glVertexAttribPointer(pos_attr, 3, GL.GL_FLOAT, GL.GL_FALSE, self._vertices.itemsize, Vertex.pos_offset())

        # vertex normals
        norm_attr = SHADER_ATTR_ID[GL.GL_VERTEX_SHADER]["norm"]
        GL.glEnableVertexAttribArray(norm_attr)
        GL.glVertexAttribPointer(norm_attr, 3, GL.GL_FLOAT, GL.GL_FALSE, self._vertices.itemsize, Vertex.norm_offset())

        # color
        col_attr = SHADER_ATTR_ID[GL.GL_VERTEX_SHADER]["col"]
        GL.glEnableVertexAttribArray(col_attr)
        GL.glVertexAttribPointer(col_attr, 4, GL.GL_FLOAT, GL.GL_FALSE, self._vertices.itemsize, Vertex.col_offset())

        # vertex texture coords
        tex_attr = SHADER_ATTR_ID[GL.GL_VERTEX_SHADER]["tex"]
        GL.glEnableVertexAttribArray(tex_attr)
        GL.glVertexAttribPointer(tex_attr, 2, GL.GL_FLOAT, GL.GL_FALSE, self._vertices.itemsize, Vertex.tex_offset())

        GL.glDrawArrays(self._mode, 0, len(self._vertices))

        GL.glBindVertexArray(0)
