#pragma once

#include <pthread.h>

namespace kinespace
{
    class PMutex
    {
    public:
        PMutex()
        {
            pthread_mutex_init(&pthread_mutex, NULL);
        }

        void lock()
        {
            pthread_mutex_lock(&pthread_mutex);
        }

        void unlock()
        {
            pthread_mutex_unlock(&pthread_mutex);
        }

        pthread_mutex_t& getRaw()
        {
            return pthread_mutex;
        }

        class PScopedLock
        {
        public:
            PScopedLock(PMutex &mutex)
                : mutex(mutex)
            {
                mutex.lock();
            }
            ~PScopedLock()
            {
                mutex.unlock();
            }
        private:
            PMutex &mutex;
        };

    private:
        pthread_mutex_t pthread_mutex;
    };

    class PCondVar
    {
    public:
        PCondVar()
        {
            pthread_cond_init(&pthread_cond_var, NULL);
        }

        int wait(PMutex &mutex, size_t miliseconds = 0)
        {
            if (miliseconds)
            {
                timespec ts;
                clock_gettime(CLOCK_REALTIME, &ts);
                ts.tv_sec += miliseconds / 1000;
                ts.tv_nsec += (miliseconds % 1000) * 1000;
                return pthread_cond_timedwait(&pthread_cond_var, &mutex.getRaw(), &ts);
            }
            return pthread_cond_wait(&pthread_cond_var, &mutex.getRaw());
        }

        int signal(bool to_all = false)
        {
            if (to_all)
            {
                return pthread_cond_broadcast(&pthread_cond_var);
            }
            return pthread_cond_signal(&pthread_cond_var);
        }

    private:
        pthread_cond_t pthread_cond_var;
    };
} // namespace kinespace
