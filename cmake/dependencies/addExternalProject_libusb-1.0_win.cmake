set(EP_LIBUSB "libusb")
set(EP_LIBUSB_TAG "" CACHE STRING "libusb version (git tag)")
set(EP_LIBUSB_SRCDIR "${CMAKE_CURRENT_BINARY_DIR}/${EP_PREFIX}/src/${EP_LIBUSB}")
set(EP_LIBUSB_BINDIR "${CMAKE_CURRENT_BINARY_DIR}/${EP_PREFIX}")

if(MSVC_TOOLSET_VERSION EQUAL "120")
set(EP_LIBUSB_MSVC_VER "2013")
elseif(MSVC_TOOLSET_VERSION EQUAL "140")
set(EP_LIBUSB_MSVC_VER "2015")
elseif(MSVC_TOOLSET_VERSION EQUAL "141")
set(EP_LIBUSB_MSVC_VER "2017")
elseif(MSVC_TOOLSET_VERSION EQUAL "142")
set(EP_LIBUSB_MSVC_VER "2019")
endif()

set(EP_LIBUSB_MSVC_BUILDCMD "\
msbuild /p:Configuration=Release \
/target:\"libusb-1.0 (dll)\" \
${EP_LIBUSB_SRCDIR}/msvc/libusb_${EP_LIBUSB_MSVC_VER}.sln")

ExternalProject_Add("${EP_LIBUSB}"
    GIT_REPOSITORY      "https://github.com/libusb/libusb.git"
    GIT_TAG             "${EP_LIBUSB_TAG}"

    BUILD_ALWAYS        OFF
    CONFIGURE_COMMAND   ""
    BUILD_COMMAND       "${EP_LIBUSB_MSVC_BUILDCMD}"
)

set(LIBUSB_1_INCLUDE_DIR "${EP_LIBUSB_BINDIR}/include/libusb-1.0" CACHE PATH "libusb-1.0 include path" FORCE)
set(LIBUSB_1_LIBRARY "${EP_LIBUSB_BINDIR}/lib/libusb-1.0.so" CACHE FILEPATH "libusb-1.0 library" FORCE)
