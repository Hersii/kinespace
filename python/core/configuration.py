import os.path as path

DIR_PATH = path.dirname(__file__)

GLSL_PATH = path.join(DIR_PATH, '..', 'graphics', 'glsl')
