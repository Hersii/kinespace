macro(add LIST_NAME ITEM)
    if(WIN32)
        string(JOIN ";" ${LIST_NAME} "${${LIST_NAME}}" "${ITEM}")
    else()
        string(JOIN ":" ${LIST_NAME} "${${LIST_NAME}}" "${ITEM}")
    endif()
endmacro()

find_package(Python)
set(PYTHONLIBS "")
if(BUILD_libfreenect)
    string(APPEND PYTHONLIBS "${EXT_LIBRARY_DIR}/python${Python_VERSION_MAJOR}.${Python_VERSION_MINOR}/site-packages")
    add(PYTHONLIBS "${CMAKE_CURRENT_SOURCE_DIR}/python")
endif()

set(LIBRARYPATH "")
string(APPEND LIBRARYPATH "${CMAKE_CURRENT_BINARY_DIR}/lib")

configure_file("${CMAKE_CURRENT_SOURCE_DIR}/cmake/templates/vscode/.env.in" "${CMAKE_CURRENT_SOURCE_DIR}/.env")
