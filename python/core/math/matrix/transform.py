from typing import Union

import numpy as np
from pyrr import Matrix33, Matrix44, Quaternion, Vector3


class Transform(Matrix44):
    def __new__(cls, value=None, dtype=np.float32):
        if value is None:
            value = np.identity(4, dtype=np.float32)
        return super().__new__(cls, value, dtype=dtype)

    @classmethod
    def from_translation_scale_rotation(cls, translation: np.matrix, scale: np.matrix, rotation: np.matrix):
        return cls(translation * scale * rotation, dtype=np.float32)

    def right_axis(self) -> Vector3:
        return np.array(self.c1[:3])

    def up_axis(self) -> Vector3:
        return np.array(self.c2[:3])

    def forward_axis(self) -> Vector3:
        return np.array(self.c3[:3])

    def get_scale(self) -> Vector3:
        return self.decompose()[0]

    def get_rotation(self) -> Quaternion:
        return self.decompose()[1]

    def get_translation(self) -> Vector3:
        return self.decompose()[2]

    def scale(self, factor: Union[int, Vector3]):
        scale_matrix = np.identity(4, dtype=np.float32)
        scale_matrix[:3, :3] *= factor
        return Transform(self * scale_matrix, dtype=np.float32)

    def rotate_by_quat(self, quat: Quaternion):
        return Transform(self * quat, dtype=np.float32)

    def rotate_by_axis_angle(self, axis: Vector3, angle: float):
        return self.rotate_by_quat(Quaternion.from_axis_rotation(axis, angle, dtype=np.float32))

    def rotate_by_matrix(self, matrix: Union[Matrix44, Matrix33]):
        return self.rotate_by_quat(Quaternion.from_matrix(matrix))

    def translate(self, vector: Vector3):
        translation = np.identity(4, dtype=np.float32)
        translation[3, :3] += vector
        return Transform(self * translation, dtype=np.float32)
