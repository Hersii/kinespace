import sys

from PyQt5 import QtGui

def popup_dialog(title, msg):
    app = QtGui.QApplication(sys.argv)
    QtGui.QMessageBox.critical(None, title, msg)
