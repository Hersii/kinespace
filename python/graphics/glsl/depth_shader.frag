#version 450
in vec4 depth;
out vec4 frag_col;

void main() { frag_col = depth; }