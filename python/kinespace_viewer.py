#!/usr/bin/env python3
import sys

from PyQt5.QtWidgets import QApplication
from PyQt5.QtGui import QOpenGLVersionProfile, QSurfaceFormat

import gui.main as main_ui

def main(args=None):
    args = args or sys.argv
    glformat = QSurfaceFormat()
    glformat.setVersion(4, 1)
    glformat.setProfile(QSurfaceFormat.CoreProfile)
    glformat.setSamples(4)
    QSurfaceFormat.setDefaultFormat(glformat)

    gl_ver_profile = QOpenGLVersionProfile()
    gl_ver_profile.setVersion(4, 1)
    gl_ver_profile.setProfile(QSurfaceFormat.CoreProfile)

    app = QApplication(args)

    main_win, log_win = main_ui.load(gl_ver_profile)
    
    main_win.show()
    log_win.show()

    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
