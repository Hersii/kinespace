set(EP_GLM "glm")
set(EP_GLM_TAG "0.9.9.8" CACHE STRING "glm version (git tag)")
get_directory_property(EP_LIBUSB_PREFIX EP_PREFIX)
set(EP_GLM_SRCDIR "${CMAKE_CURRENT_BINARY_DIR}/${EP_LIBUSB_PREFIX}/src/${EP_GLM}")
set(EP_GLM_BINDIR "${CMAKE_CURRENT_BINARY_DIR}/${EP_LIBUSB_PREFIX}")

set(EP_GLM_INSTALL_COMMAND 
rsync -ar --exclude "**/CMakeLists.txt" ${EP_GLM_SRCDIR}/glm ${EP_GLM_BINDIR}/include &&
rsync -a --include "libglm*" --exclude "**" ${EP_GLM_SRCDIR}-build/glm/ ${EP_GLM_BINDIR}/lib
)

ExternalProject_Add("${EP_GLM}"
    GIT_REPOSITORY  "https://github.com/g-truc/glm.git"
    GIT_TAG         "${EP_GLM_TAG}"
    GIT_SHALLOW     ON

    CMAKE_CACHE_ARGS
        -DGLM_TEST_ENABLE:BOOL=OFF
        -DCMAKE_BUILD_TYPE:STRING=Release
        -DCMAKE_INSTALL_PREFIX:PATH=<INSTALL_DIR>

    INSTALL_COMMAND "${EP_GLM_INSTALL_COMMAND}"
)
