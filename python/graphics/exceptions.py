class InvalidReferenceError(ReferenceError):
    """Invalid OpenGL object reference"""

class ShaderCompilationFailure(RuntimeError):
    ...

class ShaderProgramLinkFailure(RuntimeError):
    ...

class ShaderProgramValidationFailure(RuntimeError):
    ...
