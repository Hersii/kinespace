import os.path as path

import PyQt5.uic as uic

DIR_PATH = path.dirname(__file__)

DESIGN_PATH = path.join(DIR_PATH, 'design')

uic.widgetPluginPath.append(path.join(DIR_PATH, 'widgets', 'plugins'))
