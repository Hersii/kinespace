from abc import ABC, abstractmethod
from typing import Union
from pyrr.objects.matrix33 import Matrix33
from pyrr.objects.matrix44 import Matrix44
from pyrr.objects.quaternion import Quaternion

from pyrr.objects.vector3 import Vector3
from core.math.matrix.transform import Transform
from utils.log_factory import create_class_logger

class SceneObject(ABC):
    def __init__(self):
        self.logger = create_class_logger(type(self))
        self.transform = Transform() # local object transform

    @abstractmethod
    def setup(self):
        raise NotImplementedError("There is no implementation for setup().")

    def translate(self, vector: Vector3):
        self.transform = self.transform.translate(vector)

    def rotate_axis_angle(self, axis: Vector3, angle: float):
        self.transform = self.transform.rotate_by_axis_angle(axis, angle)

    def rotate_quaternion(self, quat: Quaternion):
        self.transform = self.transform.rotate_by_quat(quat)

    def rotate_matrix(self, mat: Union[Matrix44, Matrix33]):
        self.transform = self.transform.rotate_by_matrix(mat)

    def scale(self, factor: Union[float, Vector3]):
        self.transform = self.transform.scale(factor)

    def scale_separate(self, x: float, y: float, z: float):
        factor = Vector3((x, y, z))
        self.transform = self.transform.scale(factor)
