from pyrr.objects.quaternion import Quaternion
from pyrr.objects.vector3 import Vector3
import freenect
from functools import partial
import numpy as np
from core.math.matrix.transform import Transform
from core.math.vertex import Vertex


class KinectDevice:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.running = False

        self.depth_buffer = np.zeros((640, 480), dtype=np.uint16)
        self.color_buffer = np.zeros((640, 480, 3), dtype=np.uint8)

        self.transform = Transform()

    def _update_depth(self, dev, depth, timestamp):
        np.copyto(self.depth_buffer, np.transpose(depth, (1, 0)))

    def _update_color(self, dev, color, timestamp):
        np.copyto(self.color_buffer, np.transpose(color, (1, 0, 2)))

    def _update_transform(self, axes):
        up = -Vector3(list(axes), dtype=np.double)
        rotation_axis = np.cross(up, self.transform.up_axis())
        if not any(rotation_axis):
            # would cause div by zero
            return
        rotation_axis /= np.sqrt(np.sum(rotation_axis ** 2))
        rotation_angle = np.arccos(
            np.dot(up, self.transform.up_axis()) / 
            (np.sqrt(np.sum(up ** 2))*np.sqrt(np.sum(self.transform.up_axis() ** 2)))
        )
        self.transform = self.transform.rotate_by_axis_angle(rotation_axis, rotation_angle)

    def _body(self, dev, ctx):
        self._update_transform(freenect.get_accel(dev))
        if not self.running:
            print('Kill command received, shutting down KinectDevice...')
            raise freenect.Kill

    def start(self):
        self.running = True
        # runloop functions need to be bound to object instance (partial functions)
        freenect.runloop(
            depth=partial(self._update_depth, self),
            video=partial(self._update_color, self),
            body=partial(self._body, self)
        )

    def stop(self):
        self.running = False

    def _sync_update_depth(self):
        depth, timestamp = freenect.sync_get_depth()
        self._update_depth(None, depth, timestamp)

    def _sync_update_color(self):
        video, timestamp = freenect.sync_get_video()
        self._update_color(None, video, timestamp)

    def sync_stop(self):
        freenect.sync_stop()

    def get_transform(self):
        return np.copy(self.transform)

    def get_depth_buffer(self):
        return np.copy(self.depth_buffer)

    def get_color_buffer(self):
        return np.copy(self.color_buffer)

    def get_buffers(self):
        return (self.get_depth_buffer(), self.get_color_buffer())

    def get_projected_vertices(self):
        vertices = np.zeros((640, 480), dtype=Vertex._dtype)
        vertices["pos"][:, :, 0] = np.array(np.arange(0, 640), dtype=np.dtype([("f0", np.float32, (3,))]))["f0"]
        vertices["pos"][:, :, 1] = np.arange(0, 480)
        vertices["pos"][:, :, 2] = self.depth_buffer.reshape(640, 480)
        vertices["col"][:, :, [0, 1, 2]] = self.color_buffer
        vertices["col"][:, :, 3] = np.ones((4, 3))

        #TODO: project into a frustum and apply size calibration
        return vertices
