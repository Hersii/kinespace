try:
    from . import __extra_sites__
except ImportError:
    print("Tried to locate __extra_sites__, but it was not available")
    print("Please run cmake configure")

try:
    import cv2
except ImportError:
    print('Package "cv2" is not installed')
finally:
    del cv2
