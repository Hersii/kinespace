# compile Qt .ui files
include("compileQtUIs")

# configure pip module path
configure_file("${CMAKE_CURRENT_SOURCE_DIR}/cmake/templates/__extra_sites__.py.in"
               "${CMAKE_CURRENT_SOURCE_DIR}/python/core/__extra_sites__.py")
