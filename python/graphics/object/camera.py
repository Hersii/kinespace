import numpy as np
from pyrr.objects.vector3 import Vector3
from core.math.matrix.projection import perspective, orthographic
from graphics.object.sceneobject import SceneObject
from OpenGL import GL

from graphics.shader import ShaderProgram


class Camera(SceneObject):
    def __init__(self, viewport_width, viewport_height, fovx=60, clip_near=1e-6, clip_far=1e6, ortho=False):
        super().__init__()

        self.ortho = ortho
        self.viewport_dim = (viewport_width, viewport_height)
        self.clip_near = clip_near
        self.clip_far = clip_far
        self.fovx = fovx
        if ortho:
            self.projection = orthographic(*self.viewport_dim, clip_near, clip_far)
        else:
            self.projection = perspective(*self.viewport_dim, fovx, clip_near, clip_far)

        self.move_speed = 1
        self.rotation_speed = 1
        self.keep_horizontal = True

    def setup(self, vert_shader: ShaderProgram):
        self.logger.info("Setting up camera")
        buffers = GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT
        self.logger.debug(
            "Setting viewport size {x}x{y} and clearing buffers {}",
            buffers,
            x=self.viewport_dim[0],
            y=self.viewport_dim[1],
        )
        GL.glViewport(0, 0, *self.viewport_dim)
        GL.glClear(buffers)
        self.update_projection(vert_shader)

    def update(self, vert_shader: ShaderProgram, verbose=False):
        if verbose:
            self.logger.debug("Update uniform view matrix \n{}", self.transform)
        vert_shader.set_uniform_matrix("view", self.transform)

    def update_projection(self, vert_shader: ShaderProgram):
        self.logger.debug("Update uniform projection matrix \n{}", self.transform)
        vert_shader.set_uniform_matrix("projection", self.projection)

    def resize(self, viewport_width, viewport_height):
        self.viewport_dim = (viewport_width, viewport_height)
        GL.glViewport(0, 0, *self.viewport_dim)
        if self.ortho:
            self.projection = orthographic(*self.viewport_dim, self.clip_near, self.clip_far)
        else:
            self.projection = perspective(*self.viewport_dim, self.fovx, self.clip_near, self.clip_far)

    def set_projection(self, projection_matrix):
        np.copyto(self.projection, projection_matrix, casting="no")

    def move(self, vector: Vector3):
        self.translate(vector * self.move_speed)

    def walk(self, distance: float):
        forward = self.transform.forward_axis()
        self.move(forward * distance)

    def rotate(self, pitch: float, yaw: float):
        right = self.transform.right_axis()
        if self.keep_horizontal:
            up = Vector3((0, 1, 0))
        else:
            up = self.transform.up_axis()
        self.rotate_axis_angle(right, pitch * self.rotation_speed)
        self.rotate_axis_angle(up, yaw * self.rotation_speed)
